#include "./common/book.h"

__device__ void swapfloat(float *f) {

  float tmp;
  char *src, *dst;

  src = (char*)f;
  dst = (char*)&tmp;
  dst[0] = src[3];
  dst[1] = src[2];
  dst[2] = src[1];
  dst[3] = src[0];
  *f = tmp;

}

__global__ void swappix( float *a ) {
    int tid = blockIdx.x;    // this thread handles the data at its thread id
    swapfloat(&a[tid]);
}

int getheadsize(char *fname, int extnum)
{
  /*
    measuring the header size by counting " END " in the header 
    counting " END " instead of "END" to exclude "EXTEND" 
    some header unit stars with "END "

    extnum = 1 for .fits having no FITS extension 
    extnum = 2 for .fitz.fz and .fits.ic having FITS extensions 
  */

  int     hfact = 0;
  FILE    *fp;
  char    *header;
  char    ss[10] = " END ";
  char    *sp;
  int     headunit = 2880;
  char    leading[6];   /* the leading 5 letters in a unit */ 

  header = (char*)malloc(headunit);

  if(NULL == (fp = fopen(fname, "rb"))){
    printf("\n [%s] can not be opened here\n", fname);
    return 0;
  }

  while(extnum){
    hfact++;
    fread(header, sizeof(char), headunit, fp);
    memcpy(leading, header, 5); leading[5]='\0';

    if(strcmp(leading, "END  ") == 0){
      extnum--;
    } else {
      sp = strstr(header, ss);
      if (sp){
        extnum--;
      }
    }
  }
  fclose(fp);
  free(header);

  return hfact * 2880;

}


int main( int argc, char**argv ) {

    FILE    *fp;
    int     headsize;
    int     xsize, ysize, datanum;
    float   *data;
    float   *dev_a;

    xsize = 1024;
    ysize = 1024;
    datanum = (xsize*ysize*sizeof(float)/2880+1)*2880/sizeof(float);

    headsize = getheadsize(argv[1], 1);

    data = (float*)malloc(datanum*sizeof(float));

    fp = fopen(argv[1], "rb");
    fseek(fp, headsize, SEEK_SET);
    fread(data, sizeof(float), datanum, fp);
    fclose(fp);


    // allocate the memory on the GPU
    HANDLE_ERROR( cudaMalloc( (void**)&dev_a, datanum * sizeof(float) ) );

    // copy the arrays 'a' and 'b' to the GPU
    HANDLE_ERROR( cudaMemcpy( dev_a, data, datanum * sizeof(float),
                              cudaMemcpyHostToDevice ) );
    
    swappix<<<datanum,1>>>( dev_a );

    HANDLE_ERROR( cudaMemcpy( data, dev_a, datanum * sizeof(float),
                              cudaMemcpyDeviceToHost ) );

    // display the results
    for (int i=0; i<10; i++) {
        printf( "%f\n", data[i] );
    }

    for (int i=xsize*ysize-10; i<xsize*ysize; i++) {
        printf( "%f\n", data[i] );
    }


    // free the memory allocated on the GPU
    HANDLE_ERROR( cudaFree( dev_a ) );

    free(data);


    return 0;

}

