#include <thrust/sort.h>

int main(void)
{
    const int N = 6;
    int A[N] = {1, 4, 2, 8, 5, 7};

    thrust::sort(A, A + N);
   
    for (int i = 0; i < N; i++)	{
      printf("%d %d\n", i, A[i]);
    }

    return 0;

}
